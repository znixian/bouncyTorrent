import java.util.List;
import java.util.Map;

/**
 * Created by znix on 3/03/17.
 */
public class BEncoder {
	private BEncoder() {
	}

	public static String encode(Object object) {
		StringBuilder builder = new StringBuilder();
		encode(builder, object);
		return builder.toString();
	}

	@SuppressWarnings("unchecked")
	public static void encode(StringBuilder output, Object object) {
		if (object instanceof Map) {
			Map<String, Object> map = (Map<String, Object>) object;

			output.append('d');
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				encode(output, entry.getKey());
				encode(output, entry.getValue());
			}
			output.append('e');

		} else if (object instanceof List) {
			List<Object> list = (List<Object>) object;

			output.append('l');
			for (Object obj : list) {
				encode(output, obj);
			}
			output.append('e');

		} else if (object instanceof String) {
			String text = (String) object;

			output.append(text.length());
			output.append(':');
			output.append(text);
		} else if (object instanceof Integer) {
			output.append('i');
			output.append((int) object);
			output.append('e');
		} else {
			throw new RuntimeException("Cannot encode class " + object.getClass().getName() + " for " + object);
		}
	}
}
