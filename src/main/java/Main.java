import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by znix on 3/03/17.
 */
@SpringBootApplication
public class Main {
	private Main() {
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(TrackerCallback.class, args);
	}
}
