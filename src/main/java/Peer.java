import java.util.HashMap;
import java.util.Map;

/**
 * Represents a single peer in the swarm.
 * <p>
 * Created by znix on 3/03/17.
 */
public class Peer {
	private final String peerId;
	private final int port;
	private final long uploaded;
	private final long downloaded;
	private final long left;
	private final String ip;
	private final int numwant;
	private final String key;

	public Peer(
			String peerId,
			int port,
			long uploaded,
			long downloaded,
			long left,
			String ip,
			int numwant,
			String key
	) {
		this.peerId = peerId;
		this.port = port;
		this.uploaded = uploaded;
		this.downloaded = downloaded;
		this.left = left;
		this.ip = ip;
		this.numwant = numwant;
		this.key = key;
	}

	public boolean isSeeding() {
		return left == 0;
	}

	public boolean compatibleWith(Peer other) {
		return isSeeding() != other.isSeeding();
	}

	public Map<String, Object> encode(boolean includePeerId) {
		Map<String, Object> map = new HashMap<>();

		if (includePeerId) map.put("peer id", peerId);
		map.put("ip", ip);
		map.put("port", port);

		return map;
	}

	@Override
	public String toString() {
		return "Peer{" +
				"addr=" + ip + ':' + port +
				", left=" + left +
				", peerId=" + peerId +
				'}';
	}
// Getters

	public String getPeerId() {
		return peerId;
	}

	public int getPort() {
		return port;
	}

	public long getUploaded() {
		return uploaded;
	}

	public long getDownloaded() {
		return downloaded;
	}

	public long getLeft() {
		return left;
	}

	public String getIp() {
		return ip;
	}

	public int getNumwant() {
		return numwant;
	}

	public String getKey() {
		return key;
	}
}
