import java.util.*;

/**
 * Created by znix on 3/03/17.
 */
public class Swarm {
	private final Map<String, Peer> peers;

	public Swarm() {
		peers = new HashMap<>();
	}

	public Peer add(Peer peer) {
		return peers.put(peer.getPeerId(), peer);
	}

	public Peer remove(Peer peer) {
		return peers.remove(peer.getPeerId());
	}

	public List<Map<String, Object>> encodedPeers(Peer peer, boolean includePeerIds) {
		List<Map<String, Object>> result = new ArrayList<>();

		findPeersFor(peer).stream()
				.map(p -> p.encode(includePeerIds))
				.forEach(result::add)
		;

		return result;
	}

	public List<Peer> findPeersFor(Peer peer) {
		List<Peer> result = new ArrayList<>();

		peers.values().stream()
				.filter(peer::compatibleWith) // Only connect to compatible peers
				.filter(p -> p != peer) // Not much use connecting to itself.
				.limit(peer.getNumwant()) // Only want so many peers.
				.forEach(result::add)
		;

		return result;
	}

	// Direct delegates

	public String toString() {
		return peers.toString();
	}

	public int size() {
		return peers.size();
	}

	public boolean isEmpty() {
		return peers.isEmpty();
	}

	public void clear() {
		peers.clear();
	}

	public Collection<Peer> values() {
		return peers.values();
	}
}
