import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by znix on 3/03/17.
 */
@RestController
@EnableAutoConfiguration
public class TrackerCallback {
	private static final String STOPPED = "stopped";

	private final Map<String, Swarm> torrents;
	private final Map<String, String> mappings;

	public TrackerCallback() {
		torrents = new HashMap<>();

		mappings = new HashMap<>();
		mappings.put("127.0.0.1", "10.40.0.109");
	}

	@RequestMapping("/")
	String home() {
		return "Hello World!";
	}

	@RequestMapping("/announce")
	String announce(
			@RequestParam String info_hash,
			@RequestParam String peer_id,
			@RequestParam int port,
			@RequestParam(required = false) long uploaded,
			@RequestParam(required = false) long downloaded,
			@RequestParam long left,
			@RequestParam(required = false) String ip,
			@RequestParam(defaultValue = "30") int numwant,
			@RequestParam(required = false) String key,
			@RequestParam(required = false) String trackerid,
			@RequestParam(defaultValue = "0") int no_peer_id,
			@RequestParam(required = false) String event,
			HttpServletRequest request
	) {
		// If the client didn't supply an IP address, fill it in for them
		if (ip == null) ip = request.getRemoteAddr();

		// See if it's mapped to anything
		ip = mappings.getOrDefault(ip, ip);

		// Find the swarm
		Swarm swarm = torrents.computeIfAbsent(info_hash, v -> new Swarm());

		// Build the peer
		Peer peer = new Peer(peer_id, port, uploaded, downloaded, left, ip, numwant, key);

		if(STOPPED.equals(event)) {
			// remove the peer from the swarm
			swarm.remove(peer);
		} else {
			// Add the peer to the swarm
			swarm.add(peer);
		}

		// Start building the responce
		Map<String, Object> responce = new HashMap<>();

		responce.put("interval", 10); // TODO change for production
		responce.put("peers", swarm.encodedPeers(peer, no_peer_id != 1));

		// TODO only for debugging
		System.out.println(info_hash + ": " + swarm);

		// Encode and return the responce
		return BEncoder.encode(responce);
	}
}
